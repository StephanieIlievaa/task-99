# A React Task
* In this task we have to create a login component by using **styled-components**. In the project we are provided with styling for all the elements of the form and we will create a several styled-components.
## Objective
* Checkout che dev branch.
* Install the **styled-components** package using npm.
* In the **styles.css** file located in the components folder in the boilerplate we have all the necessaty styling for the login form.
* In the **form.js** file we have to implement styled-components. We must create **Card**, **Form**, **Input** and **Button** compoments. The **Card** component should act as a wrapper of the **Form**. The form itself should have 2 inputs - one for an email and one for a password.
* There should be a login **Button**.
## Requirements
 * The project starts with **npm run start**.
 * To install styled-components use command **npm install --save styled-components**
 * When implemented merge the dev branch into master.
## Gotchas
Read more about **styled-components** - https://styled-components.com/docs
